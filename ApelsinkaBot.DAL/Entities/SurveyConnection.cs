﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelsinkaBot.DAL.Entities
{
    public class SurveyConnection
    {
        public SurveyPoint SurveyPoint { get; set; }
        [Column("survey_point_id")]
        public int SurveyPointId { get; set; }

        public SurveyPoint ParentSurveyPoint { get; set; }
        [Column("parent_id")]
        public int ParentSurveyPointId { get; set; }
    }
}
