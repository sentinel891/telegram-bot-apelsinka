﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelsinkaBot.DAL.Entities
{
    public class SurveyPoint
    {
        [Column("id")]
        public int Id { get; set; }

        [Column("survey_text")]
        public string SurveyText { get; set; }
    }
}
