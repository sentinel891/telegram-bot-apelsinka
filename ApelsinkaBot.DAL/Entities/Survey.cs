﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelsinkaBot.DAL.Entities
{
    public class Survey
    {
        [Column("id")]
        public int Id { get; set; }

        [Column("name")]
        public string Name { get; set; }

        public Bot Bot { get; set; }
        [Column("bot_id")]
        public int BotId { get; set; }

        public SurveyPoint FirstSurveyPoint { get; set; }
        [Column("first_point_id")]
        public int FirstSurveyPointId { get; set; }
    }
}
