﻿using ApelsinkaBot.DAL.Entities;
using ApelsinkaBot.DAL.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelsinkaBot.DAL.Repositories
{
    public class BotRepository : BaseRepository<Bot>, IBotRepository
    {

    }
}
