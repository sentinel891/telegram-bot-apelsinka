﻿using ApelsinkaBot.BLL.Services.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace ApelsinkaBot.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BotController : ControllerBase
    {
        private readonly IBotService _service;

        public BotController(IBotService service)
        {
            _service = service;
        }

    }
}
