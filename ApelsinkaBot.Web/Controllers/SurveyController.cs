﻿using ApelsinkaBot.BLL.Services.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace ApelsinkaBot.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SurveyController : ControllerBase
    {
        private readonly ISurveyService _service;

        public SurveyController(ISurveyService service)
        {
            _service = service;
        }

    }
}
