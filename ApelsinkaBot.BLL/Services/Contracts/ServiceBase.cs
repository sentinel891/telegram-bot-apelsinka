﻿using ApelsinkaBot.DAL.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelsinkaBot.BLL.Services.Contracts
{
    public abstract class ServiceBase<R, T> : IServiceBase<T> where R : IBaseRepository<T>
                                                              where T : class
    {
        private R _repository;
        protected ServiceBase(R repository)
        {
            _repository = repository;
        }

        public virtual Task<T> GetBySingleId(int id)
        {
            throw new NotImplementedException();
        }

        public virtual Task<List<T>> GetByMultipleIds(IEnumerable<int> id)
        {
            throw new NotImplementedException();
        }

        public virtual Task<List<T>> GetAll()
        {
            throw new NotImplementedException();
        }

        public virtual Task Delete(T entity)
        {
            throw new NotImplementedException();
        }

        public virtual Task Insert(T entity)
        {
            throw new NotImplementedException();
        }

        public virtual Task Update(T entity)
        {
            throw new NotImplementedException();
        }
    }
}
