﻿using ApelsinkaBot.BLL.Services.Contracts;
using ApelsinkaBot.DAL.Entities;
using ApelsinkaBot.DAL.Repositories.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApelsinkaBot.BLL.Services
{
    public class SurveyService : ServiceBase<ISurveyRepository, Survey>, ISurveyService
    {
        public SurveyService(ISurveyRepository repository) : base(repository)
        {
        }
    }
}
